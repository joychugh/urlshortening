URL Shortening service.


Install and Run:

1) Install all the packages in requirements.txt

    pip install -r requirements.txt
    If you would like to run this with memcache, download and install couchbase client
    Configure a memcache bucket with a port eg. 12345 and update the config.py with the settings.

2) copy the directory urlshortening to local dir
    eg.
    /Users/joychugh/Documents/git/urlshortening

3) Create a file  <any_name>.py (run.py) one level up

    /Users/joychugh/Documents/git/run.py

    contents of run.py should be as follows (between the ****)

    ********
    from urlshortening import urlshortenapp

    if __name__ == "__main__":
        # Default port is 5000
        urlshortenapp.run(debug=True)
    ********

4) Go through the config.py to make sure everything looks good.
5) Run the python file.

Usage:

1) Create a new short URL
jchugh:git joychugh$ curl -v --header "Content-Type:application/json" --data '{"url":"http://google.com"}' http://localhost:5000/shorten


# Implementation

> Validate the URL
> If valid URL:
    Hash the URL and check if the HASH exists in the cache.
    If hash exists:
        return the tinyurl key
    else:
        Increment the counter by 1
        convert the value to base 62 (0-9A-Za-z) as key
        store the Key->URL pair in data store
        store the Hash->Key pair in cache
        return the tinyurl key

Get:
    Get the URL from the data store using key
    if found:
        return 303 See Other redirect
    if not found:
        return 404 Not Found

# Scalability Notes

> One Counter server to maintain the count - We use Count instead of just using directly hashes because hashes are long
  This one Counter server will have concurrencny control over it. Since memcached 'incr' operation is atomic, we can rely on that
  Special case when count approaching max int supported by system 2^32 or 2^64
  This case can be handled ->  When we return the key for shorturl we can prepend the server it came from
  eg, normal key: abc
  prepend key: 1abc  (1 is the server number, we can match 1 with the counter server it came from)
  This way we can have multiple count servers (This is not implemented in the code I wrote)

> Multiple memcached/nosql data stores to have key-> URL and hash-> key data stored in a cluster.
  This is a good configuration since we just deal with key value pairs, we just submit our request to the cluster and the
  hashing algorithm (such as ketama algorithm) will find us our key in the cluster. We can use nosql data stores such as
  couchbase to provide speed and redundancy + data persistence.

> Multiple web service nodes
  Talking to the cluster of nosql data stores and count servers.
  The get_key method can be instrumented to do the multiple counter approach that I mentioned before.
  Since most of the program is pretty modular it should be easy to add/modify functionality.

> Cache
  Right now the cache and the data store (cache holds the key to the actual entry in the datastore) are on the same datastore
  but they can be separated since the cache will fragment the memory/disk space in a different manner than the tiny key data store.
  So to maximize the space usage it would be better to put the cache on a different system.


Things to note
    Right now it considers www.google.com http://www.google.com google.com as different addresses, that can be improved upon.
    The rfc3987 library provides parse method (which is used in the program). This can be used to disect and re-construct
    the URL.

        >>> d = parse('http://tools.ietf.org/html/rfc3986#appendix-A',
        ...           rule='URI')
        >>> assert all([ d['scheme'] == 'http',
        ...              d['authority'] == 'tools.ietf.org',
        ...              d['path'] == '/html/rfc3986',
        ...              d['query'] == None,
        ...              d['fragment'] == 'appendix-A' ])

    Or we can do a Prefix Regex matching.