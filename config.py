__author__ = 'joychugh'


MEMCACHE_IP = "localhost"
MEMCACHE_PORT = 22222
# MEMCACHE TTL IN MilliSeconds
MEMCACHE_TTL = 500


# The memcache Instance to keep track of counter.
COUNTER_MEMCACHE_IP = "localhost"
COUNTER_MEMCACHE_PORT = 2222
COUNTER_MEMCACHE_TTL = 500


# Time to live for tinyurl in MilliSeconds 0 means infinite.
TINYURLKEY_TTL = 0

# Use in memory datastore for testing. Keep YES unless you have memcached setup.
USE_IN_MEMORY = True
VALID_SCHEMES = ["http", "https", "ftp", "ftps"]
VALID_SCHEMES_OUTPUT = ",".join(VALID_SCHEMES)
DEFAULT_SCHEME = "http"

# Using default PORT 5000
URL_SHORTEN_PATTERN = "http://{domain}:5000/{key}"
URL_SHORTEN_DOMAIN = "localhost"