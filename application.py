__author__ = 'joychugh'
from flask import Flask
from datastore.memcachedatastore import MemCacheDataStore
from datastore.inmemorydatastore import InMemoryDataStore
from datastore.counterdatastore import MemCacheCounterDataStore


app = Flask(__name__)
app.config.from_object('config')

database = None
counterdb = None


if app.config.get("USE_IN_MEMORY"):
    database = InMemoryDataStore()
    counterdb = InMemoryDataStore()
else:
    database = MemCacheDataStore(app.config.get("MEMCACHE_IP", "localhost"),
                                 app.config.get("MEMCACHE_PORT", 11211),
                                 app.config.get("MEMCACHE_TTL", 500))

    counterdb = MemCacheCounterDataStore(app.config.get("COUNTER_MEMCACHE_IP", "localhost"),
                                         app.config.get("COUNTER_MEMCACHE_PORT", 11211),
                                         app.config.get("COUNTER_MEMCACHE_TTL", 500))

#Initialize the end points
from urlshortening import get, post