__author__ = 'joychugh'
from rfc3987 import parse
from encoding import dehydrate


def validate_scheme(parse_result, application):
    scheme = parse_result["scheme"]
    if not scheme:
        scheme = application.config.get("DEFAULT_SCHEME")
    if scheme not in application.config.get("VALID_SCHEMES"):
        return False
    return True


def parse_url(url):
    try:
        parse_result = parse(url)
        return parse_result
    except ValueError:
        return None


def getkey(counterdb):
    """
    Increment the counter and return the count.
    """
    count = counterdb.increment()
    return dehydrate(count)


def get_cached_value(database, key):
    """
    Return the Key of the URL if we have already seen it before.
    """
    cached_key = database.get_key(key)
    if not database.get_key(key):
        return False
    return cached_key


def build_short_url(app, key):
    """
    Build the Short URL using the key and the domain of the server
    """
    shorturl = app.config.get("URL_SHORTEN_PATTERN")
    domain = app.config.get("URL_SHORTEN_DOMAIN")
    return shorturl.format(domain=domain, key=key)