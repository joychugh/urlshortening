__author__ = 'joychugh'
import hashlib


def generate_hash(url):
    hashobj = hashlib.md5()
    hashobj.update(url)
    return hashobj.hexdigest()
