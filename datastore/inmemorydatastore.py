__author__ = 'joychugh'
from datastore import COUNTER


class InMemoryDataStore(object):
    """
    The Data store class to provide a facade around in memory map operations.
    This will be used if memcache is not available. But just as a reminder, this will
    only work when testing. Using 1 Node.
    """

    def __init__(self):
        """
        Initialize the counter since we are using in memory for testing, we can use the same map.
        """
        self.client = {COUNTER: 0}

    def get_key(self, shorturlkey):
        value = self.client.get(shorturlkey)
        return value

    def insert_key(self, key, data):
        try:
            self.client[key] = data
        except Exception:
            return False
        return True

    def increment(self):
        val = 1 + self.client.get(COUNTER)
        self.client[COUNTER] = val
        return val