__author__ = 'joychugh'
import memcache


class MemCacheDataStore(object):
    """
    The Data store class to provide a facade around memcache operations.
    """

    def __init__(self, ip, port):
        memcache_server = "{ip}:{port}".format(ip=ip, port=port)
        self.client = memcache.Client([memcache_server])

        # To check if the server is reachable for early fail.
        self.client.get_stats()

    def get_key(self, shorturlkey):
        try:
            value = self.client.get(shorturlkey)
        except Exception:
            return None
        return value

    def insert_key(self, key, data, ttl=0):
        try:
            self.client.set(key, data, ttl)
        except Exception:
            return False
        return True

