__author__ = 'joychugh'

import memcache

__author__ = 'joy'

from datastore import COUNTER


class MemCacheCounterDataStore(object):
    """
    The Data store class to provide a facade around memcache operations.
    This will do the increment operation which is an atomic operation on memcache.
    """

    def __init__(self, ip, port):
        memcache_server = "{ip}:{port}".format(ip=ip, port=port)
        self.client = memcache.Client([memcache_server])

        # To check if the server is reachable for early fail.
        self.client.get_stats()

        # Initialize the counter
        self.client.set(COUNTER, 0)

    def increment(self):
        try:
            value = self.client.incr(COUNTER)
        except Exception:
            return value
        return None

