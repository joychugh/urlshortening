__author__ = 'joychugh'
from utils.utils import validate_scheme, parse_url, getkey, get_cached_value, build_short_url
from utils.hashingutils import generate_hash
from application import app, database, counterdb
from flask import redirect, jsonify, make_response, request


@app.route("/shorten/", methods=["POST"])
@app.route("/shorten", methods=["POST"])
def post():
    args = request.get_json()
    url = args["url"]
    parse_result = parse_url(url)
    if not parse_result:
        return make_response(jsonify({"error": "Invalid URL"}), 400)
    if not validate_scheme(parse_result, app):
        return make_response(jsonify({"error": "Protocol Not Supported. Supported Protocols: {prot}".format(
            prot=app.config.get("VALID_SCHEMES_OUTPUT"))}), 400)
    hexdigest = generate_hash(url)

    # If we see the same URL again, then we can return the key we generated for it.
    key = get_cached_value(database, hexdigest)
    if key:
        return make_response(jsonify({"shorturl": build_short_url(app, key)}), 200)

    # If we see this url for first time
    key = getkey(counterdb)
    if not database.insert_key(key, url):
            return make_response(jsonify({"error": "Could not register tinyurl"}), 500)
    # Store this hash(url)-> key so that if we see same url again we dont incr the counter.
    database.insert_key(hexdigest, key)
    return make_response(jsonify({"shorturl": build_short_url(app, key)}), 200)


@app.route("/<string:key>", methods=["GET"])
def get(key):
    url = database.get_key(key)
    if not url:
        return make_response(jsonify({"error": "Not Found"}), 404)
    else:
        return redirect(url, 303)
